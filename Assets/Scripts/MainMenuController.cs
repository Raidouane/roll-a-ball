﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
	public GameObject PauseMenu;
	public GameObject WinMessage;

	public void PauseGame()
	{
		Time.timeScale = 0;
		PauseMenu.gameObject.SetActive(true);
		WinMessage.gameObject.SetActive(false);
	}

	public void ContinueGame()
	{
		ActiveTime();
		PauseMenu.gameObject.SetActive(false);
	}

	public void GoToMainMenu()
	{
		ActiveTime();
		SceneManager.LoadScene(0);
	}

	public void GoToGameField()
	{
		ActiveTime();
		SceneManager.LoadScene(1);
	}

	public void Exit()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}

	private void ActiveTime()
	{
		Time.timeScale = 1;
	}
}
