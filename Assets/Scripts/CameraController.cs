﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	#region Public Section

	public GameObject Player;

	#endregion

	#region Private Section

	private Vector3 _offset;

	void Start ()
	{
		_offset = transform.position - GetPlayerPosition();
	}

	void LateUpdate ()
	{
		transform.position = GetPlayerPosition() + _offset;
	}

	private Vector3 GetPlayerPosition()
	{
		return Player.transform.position;
	}

	#endregion
}
