﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	#region Public Section

	public float	Speed;
	public Text		CounterText;
	public Text		WinMessage;

	#endregion


	#region Private Section

	private Rigidbody	_rigidbody;
	private uint		_counter;

	private void Start()
	{
		_rigidbody = GetComponent<Rigidbody>();
		if (Math.Abs(Speed) < 0.1f)
		{
			Speed = 5.0f;
			_counter = 0;
			SetCounterText();
		}
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);
		_rigidbody.AddForce(movement * Speed);
	}

	void OnTriggerEnter(Collider other)
	{
		if (!other.gameObject.CompareTag("PickUp")) return;

		other.gameObject.SetActive(false);
		_counter++;
		SetCounterText();
		if (_counter >= 11)
		{
			WinMessage.gameObject.SetActive(true);
		}
	}

	void SetCounterText()
	{
		CounterText.text = "Count: " + _counter;
	}

	#endregion
}
